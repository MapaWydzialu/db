ALTER TABLE `PersonInRooms`
ADD FOREIGN KEY `FK_Person_ID` (`PersonID`)
REFERENCES `Persons` (`ID`);

ALTER TABLE `PersonInRooms`
ADD FOREIGN KEY `FK_Room_ID` (`RoomID`)
REFERENCES `Rooms` (`ID`);