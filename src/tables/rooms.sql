CREATE OR REPLACE TABLE `Rooms` (
  `ID`					BIGINT(20)		  UNSIGNED  NOT NULL	AUTO_INCREMENT,
  `Name`				VARCHAR(250)						  NOT NULL,
  `RoomNumber`  VARCHAR(250)              NOT NULL,
  `RoomTypeID`	BIGINT(20)		  UNSIGNED	NOT NULL,
  `x`           DECIMAL(100,2)  SIGNED    NOT NULL  DEFAULT 0,
  `y`           DECIMAL(100,2)  SIGNED    NOT NULL  DEFAULT 0,
  `z`           DECIMAL(100,2)  SIGNED    NOT NULL  DEFAULT 0,
  PRIMARY KEY (`ID`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1;
