import { Gulpclass, Task, SequenceTask } from 'gulpfile.ts/Annotations';
import * as gulp from 'gulp';
import * as mariadb from 'gulp-mariadb';
import * as fs from "fs";

var credentials = JSON.parse(fs.readFileSync("./credentials.json").toString('utf8'));

var automaria: mariadb = new mariadb({
  connection: credentials
});

@Gulpclass()
export class Gulpfile {
  @Task()
  database() {
    return gulp.src('src/database.sql')
      .pipe(automaria());
  }

  @Task()
  tables() {
    return gulp.src('src/tables/**/*.sql')
      .pipe(automaria('facultyMap'));
  }

  @Task()
  foreignkeys() {
    return gulp.src('src/foreignkeys/**/*.sql')
      .pipe(automaria('facultyMap'));
  }

  @Task()
  procedures() {
    return gulp.src('src/procedures/**/*.sql')
      .pipe(automaria('facultyMap'));
  }

  @Task()
  functions() {
    return gulp.src('src/functions/**/*.sql')
      .pipe(automaria('facultyMap'));
  }

  @Task()
  triggers() {
    return gulp.src('src/triggers/**/*.sql')
      .pipe(automaria('facultyMap'));
  }
}
